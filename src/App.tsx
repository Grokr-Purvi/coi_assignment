import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { Register } from "./Components/Auth/Register";
import Login from "./Components/Auth/Login";
import UserDashboard from "./Components/MenuItems/Dashboard/UserDashboard";
import { Provider } from "react-redux";
import { store } from "./Components/app/store";
import { SideBar } from "./Components/SideBar/SideBar";
import { TopBar } from "./Components/TopBar/TopBar";
// import { Events } from "./Components/MenuItems/Events/Events";
// import { Energy } from "./Components/MenuItems/Energy/Energy";
// import { AssetRegistory } from "./Components/MenuItems/AssetRegistry/AssetRegistry";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" component={Login} />

          <Route exact path="/register" component={Register} />
          <Route
            exact
            path="/"
            component={() => <Redirect to={"/register"} />}
          />
          <div>
            <TopBar />
            <div style={{ display: "flex" }}>
              <SideBar />
              <Route
                exact
                path="/dashboard1"
                component={() => <UserDashboard />}
              />
              {/* <Route exact path="/events" component={() => <Events />} />
              <Route exact path="/energy" component={() => <Energy />} />
              <Route
                exact
                path="/asset_registry"
                component={() => <AssetRegistory />}
              /> */}
            </div>
          </div>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
