import ReactDOM from "react-dom";
import { SideBar, getIconForMenuItem } from "./SideBar";
import WindowIcon from "@mui/icons-material/Window";
import StorageIcon from "@mui/icons-material/Storage";
import PowerIcon from "@mui/icons-material/Power";
import EnergySavingsLeafIcon from "@mui/icons-material/EnergySavingsLeaf";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<SideBar />, div);
});

test("correct icon is sent for each menu item", () => {
  expect(getIconForMenuItem("Dashboard")).toStrictEqual(<WindowIcon />);
});

test("correct icon is sent for each menu item", () => {
  expect(getIconForMenuItem("Events")).toStrictEqual(<EnergySavingsLeafIcon />);
});

test("correct icon is sent for each menu item", () => {
  expect(getIconForMenuItem("Asset Registry")).toStrictEqual(<StorageIcon />);
});

test("correct icon is sent for each menu item", () => {
  expect(getIconForMenuItem("Energy")).toStrictEqual(<PowerIcon />);
});

test("correct icon is sent for each menu item", () => {});
