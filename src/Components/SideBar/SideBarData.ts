const getData = () => {
  return [
    {
      title: "Dashboard",
      subtitle: "Your starting point",
      link: "/dashboard1",
    },
    {
      subtitle: "Your asset database",
      title: "Asset Registry",
      link: "/asset_registry",
    },
    {
      subtitle: "Real time & historical usage",
      title: "Energy",
      link: "/energy",
    },
    {
      subtitle: "Demand Response",
      title: "Events",
      link: "/events",
    },
  ];
};

function getSideBarData() {
  return getData();
}

export default getSideBarData;
