import getSideBarData from "./SideBarData";

test("test side bar menu items", () => {
  expect(getSideBarData().length).toBeGreaterThanOrEqual(0);
});
