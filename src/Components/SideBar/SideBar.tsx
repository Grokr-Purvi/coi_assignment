import {
  Box,
  List,
  ListItemButton,
  ListItemIcon,
  Typography,
  Grid,
  SvgIconTypeMap,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { useHistory } from "react-router-dom";
import useWindowDimensions from "../../Hooks/WindowsDimensions";
import NoiseControlOffIcon from "@mui/icons-material/NoiseControlOff";
import { ReactNode, useEffect, useState } from "react";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import getSideBarData from "./SideBarData";
import WindowIcon from "@mui/icons-material/Window";
import StorageIcon from "@mui/icons-material/Storage";
import PowerIcon from "@mui/icons-material/Power";
import EnergySavingsLeafIcon from "@mui/icons-material/EnergySavingsLeaf";

export interface sideBarDataType {
  title: string;
  subtitle: string;
  link: string;
}
export const getIconForMenuItem = (title: any) => {
  if (title === "Dashboard") {
    return <WindowIcon />;
  } else if (title === "Asset Registry") {
    return <StorageIcon />;
  } else if (title === "Energy") {
    return <PowerIcon />;
  } else if (title === "Events") {
    return <EnergySavingsLeafIcon />;
  }
};

export const SideBar = () => {
  const { width, height } = useWindowDimensions();
  const [selected, setSelected] = useState(0);
  const [SideBarData, setSideBarData] = useState<sideBarDataType[]>([]);

  const history = useHistory();

  const StyledList = styled(List)({
    "&& .Mui-selected, && .Mui-selected:hover": {
      backgroundColor: "red",
      "&, & .MuiListItemIcon-root": {
        color: "pink",
      },
    },
    // hover states
    "& .MuiListItemButton-root:hover": {
      backgroundColor: "#ffffff",
      "&, & .MuiListItemIcon-root": {
        color: "#2fc6a6",
      },
    },
  });

  useEffect(() => {
    var data = getSideBarData();
    console.log(data);
    setSideBarData(data);
  }, []);

  return (
    <Box
      style={{
        height: height,
        width: width * 0.2,
        backgroundColor: "#f5f9fc",
        marginLeft: "-10px",
        marginTop: "-10px",
        marginRight: "10px",
        position: "fixed",
      }}
    >
      <Typography
        align="center"
        color="#4f9dcb"
        sx={{ marginTop: "25px", marginBottom: "25px" }}
        fontSize={25}
        fontWeight={800}
      >
        OPTIMIZE
      </Typography>
      <StyledList
        sx={{
          width: "100%",
          maxWidth: 360,
          marginTop: "-10px",
        }}
      >
        {SideBarData.map((val, key) => {
          return (
            <>
              <ListItemButton
                sx={{
                  height: "80px",
                  backgroundColor: key === selected ? "white" : "#f5f9fc",
                }}
                onClick={() => {
                  setSelected(key);
                  history.push(val.link);
                }}
              >
                <ListItemIcon sx={{ marginTop: "3px" }}>
                  {getIconForMenuItem(val.title)}
                </ListItemIcon>
                <div>
                  <Typography
                    color="#bfc6cc"
                    fontWeight={600}
                    fontSize={10}
                    sx={{ marginBottom: "5px" }}
                  >
                    {key === selected ? "" : val.subtitle}
                  </Typography>
                  <div style={{ display: "flex" }}>
                    <Grid container sx={{ width: "100%" }}>
                      <Grid item md={11} xs={12}>
                        <Typography
                          color="#2A547E"
                          sx={{ width: "170px " }}
                          fontWeight={740}
                          fontSize={key === selected ? 19 : 16}
                        >
                          {val.title}
                        </Typography>
                      </Grid>
                      <Grid
                        item
                        md={1}
                        sx={{
                          display: { xs: "none", lg: "block", xl: "none" },
                        }}
                      >
                        <Box sx={{ width: "40px" }}>
                          <ListItemIcon sx={{ marginTop: "6px" }}>
                            {key === selected ? (
                              <ChevronRightIcon fontSize="small" />
                            ) : (
                              <NoiseControlOffIcon fontSize="small" />
                            )}
                          </ListItemIcon>
                        </Box>
                      </Grid>
                    </Grid>
                  </div>
                </div>
              </ListItemButton>
            </>
          );
        })}
      </StyledList>
    </Box>
  );
};
