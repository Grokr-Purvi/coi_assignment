import Button from "@mui/material/Button";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { store } from "../../app/store";
import { loggedOut } from "../../app/Actions";
import useWindowDimensions from "../../../Hooks/WindowsDimensions";
import { Box, display } from "@mui/system";
import { ButtonBase, Card, Grid, Typography } from "@mui/material";
import GraphImage from "../../../assets/graphImage.jpg";
import { PerformanceIndicators } from "./PerformanceIndicators";
import { Actions } from "./Actions";

const UserDashboard = () => {
  const { width, height } = useWindowDimensions();

  if (store.getState().loggedIn === true) {
    console.log(store.getState());
    console.log("login action set true");

    return (
      <>
        <div
          style={{
            width: width,
            height: "100%",
            marginLeft: "-10px",
            marginRight: "-10px",
            marginTop: "-10px",
            marginBottom: "-20px",
            display: "flex",
            backgroundColor: "#f5f9fc",
          }}
        >
          <div
            style={{
              width: width * 0.8,
              height: "100%", //height * 1.2,
              backgroundColor: "#f5f9fc",
              marginLeft: width * 0.25,
              marginTop: height * 0.05,
              display: "inline",
            }}
          >
            <Grid
              container
              spacing={2}
              sx={{
                paddingLeft: "40px",
                marginTop: "30px",
                // marginLeft: "50px",
              }}
            >
              <Grid container item md={1.5} justifyContent="flex-start">
                <Box>
                  <img
                    style={{
                      height: "120px",
                      width: "120px",
                      // marginRight: "10px",
                    }}
                    src={GraphImage}
                  ></img>
                </Box>
              </Grid>
              <Grid item md={4}>
                <Box sx={{ marginLeft: "20px" }}>
                  <Typography
                    sx={{ marginTop: "5px" }}
                    fontSize={40}
                    fontWeight={1000}
                  >
                    Capacity
                  </Typography>
                  <Typography
                    sx={{ marginTop: "-20px" }}
                    fontSize={40}
                    fontWeight={1000}
                  >
                    Dashboard
                  </Typography>
                </Box>
              </Grid>
            </Grid>
            <PerformanceIndicators />
            <Actions />
          </div>
        </div>
      </>
    );
  } else {
    return <Redirect to="login" />;
  }
};

export default UserDashboard;
