import { Grid, Box, Typography, ButtonBase, Card } from "@mui/material";

export const Actions = () => {
  return (
    <>
      <Grid
        container
        sx={{
          marginTop: "20px",
          marginBottom: "20px",
        }}
        spacing={2}
      >
        <Grid item md={12}>
          <Box sx={{ paddingLeft: "50px" }}>
            <Typography
              align="left"
              fontSize={12}
              color="#84919a"
              fontWeight={550}
            >
              Actions
            </Typography>
          </Box>
        </Grid>

        <Grid container item md={4} sx={{ marginLeft: "50px" }}>
          <ButtonBase>
            <Card
              sx={{
                width: "230px",
                height: "170px",
                padding: "15px",
                display: "flex",
              }}
            >
              <Box sx={{ marginTop: "40px" }}>
                <Typography
                  align="left"
                  fontSize={15}
                  color="#22598b"
                  fontWeight={550}
                >
                  Demand Response
                </Typography>
                <Typography
                  align="left"
                  fontSize={40}
                  color="#22598b"
                  fontWeight={560}
                >
                  Enroll
                </Typography>
              </Box>
            </Card>
          </ButtonBase>
        </Grid>
        <Grid container item md={4}>
          <ButtonBase>
            <Card
              sx={{
                width: "230px",
                height: "170px",
                padding: "15px",
                display: "flex",
                marginLeft: "-25px",
              }}
            >
              <Box sx={{ marginTop: "40px" }}>
                <Typography
                  align="left"
                  fontSize={15}
                  color="#22598b"
                  fontWeight={550}
                >
                  Assets
                </Typography>
                <Typography
                  align="left"
                  fontSize={40}
                  color="#22598b"
                  fontWeight={560}
                >
                  Register
                </Typography>
              </Box>
            </Card>
          </ButtonBase>
        </Grid>
      </Grid>
    </>
  );
};
