import ReactDOM from "react-dom";
import { PerformanceIndicators } from "./PerformanceIndicators";

test("component renders without fail", () => {
  const div = document.createElement("div");
  ReactDOM.render(<PerformanceIndicators />, div);
});
