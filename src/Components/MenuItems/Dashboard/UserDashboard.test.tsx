import ReactDOM from "react-dom";
import UserDashboard from "./UserDashboard";
import { render as rtlRender } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../../app/store";
import { BrowserRouter } from "react-router-dom";

const render = (component: any) =>
  rtlRender(<Provider store={store}>{component}</Provider>);

test("Dashboard renders without fail", () => {
  const div = document.createElement("div");
  render(
    <BrowserRouter>
      <UserDashboard />
    </BrowserRouter>
  );
});
