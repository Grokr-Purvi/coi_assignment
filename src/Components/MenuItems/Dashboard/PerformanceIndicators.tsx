import { Grid, Box, Typography, ButtonBase, Card } from "@mui/material";

export const PerformanceIndicators = () => {
  return (
    <Grid
      container
      sx={{
        marginTop: "20px",
      }}
      spacing={2}
    >
      <Grid item md={12}>
        <Box sx={{ paddingLeft: "50px" }}>
          <Typography
            align="left"
            fontSize={12}
            color="#22598b"
            fontWeight={550}
          >
            Key performance indicators
          </Typography>
        </Box>
      </Grid>

      <Grid container item md={3} sx={{ marginLeft: "50px" }}>
        <ButtonBase>
          <Card
            sx={{
              width: "230px",
              height: "40px",
              padding: "15px",
              display: "flex",
            }}
          >
            <Typography
              align="left"
              fontSize={12}
              color="#22598b"
              fontWeight={550}
              sx={{ width: "90%" }}
            >
              Available<br></br>Credits
            </Typography>
            <Typography
              fontSize={30}
              color="#22598b"
              fontWeight={1000}
              alignContent="center"
              sx={{ marginTop: "-5px" }}
            >
              0
            </Typography>
          </Card>
        </ButtonBase>
      </Grid>
      <Grid container item md={3} sx={{ marginLeft: "50px" }}>
        <ButtonBase>
          <Card
            sx={{
              width: "230px",
              height: "40px",
              padding: "15px",
              display: "flex",
            }}
          >
            <Typography
              align="left"
              fontSize={12}
              color="#22598b"
              fontWeight={550}
              sx={{ width: "90%" }}
            >
              Asset<br></br>Registery
            </Typography>
            <Typography
              fontSize={30}
              color="#22598b"
              fontWeight={1000}
              alignContent="center"
              sx={{ marginTop: "-5px" }}
            >
              0
            </Typography>
          </Card>
        </ButtonBase>
      </Grid>
      <Grid container item md={3} sx={{ marginLeft: "50px" }}>
        <ButtonBase>
          <Card
            sx={{
              width: "230px",
              height: "40px",
              padding: "15px",
              display: "flex",
            }}
          >
            <Typography
              align="left"
              fontSize={12}
              color="#22598b"
              fontWeight={550}
              sx={{ width: "90%" }}
            >
              Carbon<br></br>Reduction
            </Typography>
            <Typography
              fontSize={30}
              color="#22598b"
              fontWeight={1000}
              alignContent="center"
              sx={{ marginTop: "-5px" }}
            >
              0
            </Typography>
          </Card>
        </ButtonBase>
      </Grid>
      <Grid container item md={3} sx={{ marginLeft: "50px" }}>
        <ButtonBase>
          <Card
            sx={{
              width: "230px",
              height: "40px",
              padding: "15px",
              display: "flex",
            }}
          >
            <Typography
              align="left"
              fontSize={12}
              color="#22598b"
              fontWeight={550}
              sx={{ width: "90%" }}
            >
              Revenue<br></br>YTD
            </Typography>
            <Typography
              fontSize={30}
              color="#22598b"
              fontWeight={1000}
              alignContent="center"
              sx={{ marginTop: "-5px" }}
            >
              0
            </Typography>
          </Card>
        </ButtonBase>
      </Grid>
    </Grid>
  );
};
