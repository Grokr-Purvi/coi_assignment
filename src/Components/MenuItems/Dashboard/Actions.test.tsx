import ReactDOM from "react-dom";
import { Actions } from "./Actions";

test("component renders without fail", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Actions />, div);
});
