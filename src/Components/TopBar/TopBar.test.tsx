import ReactDOM from "react-dom";
import { getTime, TopBar } from "./TopBar";
import { render as rtlRender } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../app/store";

const render = (component: any) =>
  rtlRender(<Provider store={store}>{component}</Provider>);

test("topBar renders without fail", () => {
  render(<TopBar />);
});

// test("get current time works properly", () => {
//   let newDate = new Date();
//   let hours = String(newDate.getHours());
//   let minutes = String(newDate.getMinutes());
//   let seconds = String(newDate.getSeconds());

//   if (hours.length < 2)
//     expect(getTime(hours, minutes, seconds).charAt(0)).toBe("0");

//   if (hours.length == 2)
//     expect(getTime(hours, minutes, seconds).charAt(0)).not.toBe("0");

//   if (hours.length === 2 && minutes.length < 2)
//     expect(getTime(hours, minutes, seconds).charAt(3)).toBe("0");

//   if (hours.length === 2 && minutes.length === 2 && seconds.length < 2) {
//     expect(getTime(hours, minutes, seconds).charAt(6)).toBe("0");
//   }
// });
