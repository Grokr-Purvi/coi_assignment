import { Button, Box, Typography, Icon } from "@mui/material";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import useWindowDimensions from "../../Hooks/WindowsDimensions";
import { loggedOut } from "../app/Actions";
import Brightness5Icon from "@mui/icons-material/Brightness5";
import { useState, useEffect } from "react";
import { useDispatchApp } from "../app/redux-hooks";

export const getTime = () => {
  let newDate = new Date();
  let hours = String(newDate.getHours());
  let minutes = String(newDate.getMinutes());
  let seconds = String(newDate.getSeconds());
  var ampm = parseInt(hours) >= 12 ? "pm" : "am";

  if (minutes.length < 2) {
    minutes = "0" + minutes;
  }

  if (hours.length < 2) {
    hours = "0" + hours;
  }

  if (seconds.length < 2) {
    seconds = "0" + seconds;
  }

  return hours + ":" + minutes + ":" + seconds + " " + ampm;
};

export const TopBar = () => {
  const { width, height } = useWindowDimensions();

  const history = useHistory();
  const dispatch = useDispatchApp();

  const handleLogOut = () => {
    dispatch(loggedOut());
    history.push("login");
  };

  const getDate = () => {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = String(newDate.getFullYear());
    year = year.slice(year.length - 2);

    var dateFinal = date + "." + month + "." + year;
    return dateFinal;
  };

  const [dt, setDt] = useState(getTime());

  useEffect(() => {
    let secTimer = setInterval(() => {
      setDt(getTime());
    }, 1000);

    return () => clearInterval(secTimer);
  }, []);

  return (
    <Box
      style={{
        height: height * 0.11,
        width: width,
        backgroundColor: "#f5f9fc",
        marginTop: "-10px",
        marginLeft: "-10px",
        marginRight: "-10px",
        marginBottom: "5px",
        position: "fixed",
      }}
    >
      <Box
        sx={{
          display: "flex",
          width: "430px",
          backgroundColor: "white",
          height: height * 0.07,
          float: "right",
          marginRight: "30px",
          marginTop: "20px",
          borderRadius: 20,
        }}
      >
        <Icon sx={{ marginTop: "10px", marginLeft: "15px" }}>
          <Brightness5Icon />
        </Icon>
        <Typography
          sx={{ marginTop: "13px", marginLeft: "10px" }}
          fontWeight={700}
          fontSize={13}
          color="#7f8992"
        >
          Welcome Back, Abc
        </Typography>
        <Typography
          sx={{ marginTop: "13px", marginLeft: "10px" }}
          fontWeight={700}
          fontSize={13}
          color="#b4bdbf"
        >
          {getDate()} | {dt}
        </Typography>
        <Button
          style={{
            textTransform: "none",
            marginLeft: "30px",
            marginTop: "1.5px",
            background: "transparent",
            // color: "#b4bdbf",
          }}
          variant="text"
          size="small"
          onClick={handleLogOut}
        >
          <b>Sign Out</b>
        </Button>
      </Box>
    </Box>
  );
};
