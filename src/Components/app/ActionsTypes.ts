export const INVALID_EMAIL = "invalid email";
export const EMPTY_EMAIL = "empty email";
export const EMPTY_PASS = "empty password";
export const EMPTY_CON_PASS = "empty confirm password";
export const PASS_DO_NOT_MATCH = "passwords do not match";
export const REGISTER = "register";
export const PASS_LENGTH = "password length is less";
export const VALID_PASS = "password is valid";
export const VALID_EMAIL = "email is valid";
export const VALID_CON_PASS = "confirm password is valid";

export const LOGGED_IN = "logged in";
export const LOGGED_OUT = "logged out";
