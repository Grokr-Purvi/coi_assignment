import {
  InvalidEmail,
  EmptyEmail,
  EmptyPass,
  EmptyConPass,
  PassDoNotMatch,
  PassLength,
  ResetErrors,
  LoggedIn,
  LoggedOut,
} from "./Actions";

export interface errorsState {
  errors: string[];
  loggedIn: boolean;
}

const initialState = {
  errors: [],
  loggedIn: false,
};

type Action =
  | InvalidEmail
  | EmptyEmail
  | EmptyPass
  | EmptyConPass
  | PassDoNotMatch
  | PassLength
  | ResetErrors
  | LoggedIn
  | LoggedOut;

export const Reducer = (state: errorsState = initialState, action: Action) => {
  switch (action.type) {
    case "RESET_ERRORS": {
      return { ...state, errors: [] };
    }
    case "INVALID_EMAIL": {
      console.log("invalid email");
      return { ...state, errors: [...state.errors, action.type] };
    }
    case "EMPTY_EMAIL": {
      console.log("empty email case");
      return { ...state, errors: [...state.errors, action.type] };
    }
    case "EMPTY_PASS": {
      return { ...state, errors: [...state.errors, action.type] };
    }
    case "EMPTY_CONFIRM_PASS": {
      return { ...state, errors: [...state.errors, action.type] };
    }

    case "PASS_LENGTH": {
      return { ...state, errors: [...state.errors, action.type] };
    }
    case "PASS_DO_NOT_MATCH": {
      return { ...state, errors: [...state.errors, action.type] };
    }

    case "LOGGED_IN": {
      console.log("case LOGGED_IN");
      return { ...state, loggedIn: true };
    }

    case "LOGGED_OUT": {
      console.log("case logged out");
      return { ...state, loggedIn: false };
    }

    default: {
      return state;
    }
  }
};
