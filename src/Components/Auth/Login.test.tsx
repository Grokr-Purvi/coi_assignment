import Login from "./Login";
import { render as rtlRender } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../app/store";

const render = (component: any) =>
  rtlRender(<Provider store={store}>{component}</Provider>);

it("Login Page renders successfully", () => {
  render(<Login />);
});

it("Handle login works properly", () => {
  // expect(Login.)
});
