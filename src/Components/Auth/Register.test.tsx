import Login from "./Login";
import { render as rtlRender } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../app/store";
import { Register } from "./Register";

const render = (component: any) =>
  rtlRender(<Provider store={store}>{component}</Provider>);

it("Register Page renders successfully", () => {
  render(<Register />);
});
